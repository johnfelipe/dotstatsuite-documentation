> **Thank you for your interest in our project**. 
We welcome your interest and you can explore further to know if you would like to engage with our open source Community.  
You can find out more about the Community on our [web site](https://siscc.org). To help us continuously improve this documentation site, you can send suggestions by email to [contact@siscc.org](mailto:contact@siscc.org?subject=documentation-suggestion).

### What is .Stat Suite?

**An environment to manage the data lifecycle for official stats** (design, collect, process, disseminate).  
**An environment to explore data** and develop various reporting and dissemination experiences.  
**An ‘SDMX-native’ environment**, building on best practices in statistical data modelling.  

![homepage splash](/dotstatsuite-documentation/images/home_splash.png)

[![Mentioned in Awesome Official Statistics ](https://awesome.re/mentioned-badge.svg)](http://www.awesomeofficialstatistics.org)
