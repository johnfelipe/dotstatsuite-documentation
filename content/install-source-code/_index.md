---
title: "Installing .Stat Suite from source code"
subtitle: 
comments: false
weight: 40

---

The following pages provide examples of topologies (with targetted infrastructures) and the related required .Stat Suite platform components (applications, services, libraries), which can be freely re-used to compose a new topology (system architecture) by anyone on their own infrastructure. Also included are the links to the related instructions for installations starting from the source code.  

This approach requires solid technical knowledge of the underlying ecosystems (.Net, MS SQL Server, JS, OS) but it offers the most flexibility for topologies and configuration.  

You can also have a look at the [Delivery and support streams' diagram](https://sis-cc.gitlab.io/dotstatsuite-documentation/getting-started/) for a better understanding of the installation approach.
