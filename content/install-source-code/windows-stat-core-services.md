---
title: "Source code Windows installation example of .Stat Core services"
subtitle: 
comments: false
weight: 44
keywords: [
  'Disclaimer', '#disclaimer',
  'Pre-requisites', '#pre-requisites',
  'Topology', '#topology',
  'Installation overview', '#installation-overview',
  '1. Download the source code', '#1-download-the-source-code',
  '2. Compile the source code', '#2-compile-the-source-code',
  '3. Initialize the databases', '#3-initialize-the-databases',
  '4. Deploy the Transfer service', '#4-deploy-the-transfer-service',
  '5. Deploy the Design NSI web service in port 81', '#5-deploy-the-design-nsi-web-service-in-port-81',
  '6. Deploy the Disseminate NSI web service in port 80', '#6-deploy-the-disseminate-nsi-web-service-in-port-80',
  '7. Additional IIS configuration', '#7-additional-iis-configuration',
]
---

#### Table of Content
- [Disclaimer](#disclaimer)
- [Pre-requisites](#pre-requisites)
- [Topology](#topology)
- [Installation overview](#installation-overview)
- [1. Download the source code](#1-download-the-source-code)
- [2. Compile the source code](#2-compile-the-source-code)
- [3. Initialize the databases](#3-initialize-the-databases)
- [4. Deploy the Transfer service](#4-deploy-the-transfer-service)
- [5. Deploy the Design NSI web service in port 81](#5-deploy-the-design-nsi-web-service-in-port-81)
- [6. Deploy the Disseminate NSI web service in port 80](#6-deploy-the-disseminate-nsi-web-service-in-port-80)
- [7. Additional IIS configuration](#7-additional-iis-configuration)

The following example contains the list of steps required to deploy a **specific topology** of the dotstatsuite-core components. The configuration of the components has been **predefined with default values** to ease the installation process. The installation process is based on **Git Bash commands** as a way to standardize and reduce the installation steps.

---

### Disclaimer

> **`This example SHOULD NOT be used as is for production deployments.`**
> 
> In this installation example all sensitive information is set to use default values, including connection strings, SQL users and passwords, database names, application folders, among others.  
>  **This information is publically available, making your deployment vulnerable.**

---

### Pre-requisites
> Microsoft .NET Core 6.0.\* required for source code installation since [June 28, 2022 Release](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#june-28-2022) 

Make sure that the windows machine which will be used in this installation process, has all the following components already installed, and that you have all the required information.

- **IIS Web server**  
    - IIS server 7.5 or later  
    - ASP.Net application roles/features enabled  
    - Microsoft Visual C++ 2015 Redistributable [download](https://www.microsoft.com/en-US/download/details.aspx?id=52685) 

- **SQL server 2017** or higher
    - SQL authentication enabled  
    - Named pipes enabled  
    - SQL browser service running  
    - SQL Server Agent running  
    - User and password with **sysadmin** role  

- **Microsoft .NET**      
	- Microsoft .NET Core SDK 6.0.\* (x64 version) [download](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
	- Microsoft .NET Core 6.0.\* - Windows Server Hosting Bundle [download](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)  (IIS must be restarted after installation)
	- (Optional) Visual studio 2022 is to compile solutions with .net core 6.0

- **Git for windows** (x64 version) [download](https://git-scm.com/download/win).  

> **`WARNING!`** - **`This installation example will fail if any of the pre-requisites is missing.`**  


> **As a general rule, we suggest to limit the amount of memory allocated to the MSSQL server**; A maximum of 75% of the total memory should be allocated to MSSQL.
> - For example, based on the [recommended minimum requirements](https://sis-cc.gitlab.io/dotstatsuite-documentation/getting-started/infrastructure-requirements/) of at total of 16GB, MSSQL server should use a maximum of 10GB. [See more how to set this limit in the official documentation](https://docs.microsoft.com/en-us/sql/database-engine/configure-windows/server-memory-server-configuration-options?view=sql-server-ver15)
---

### Topology

![.Stat Core topology two spaces](/dotstatsuite-documentation/images/stat-core-topology-two-spaces.PNG)

---

### Installation overview
1.   [Download the source code](#1-download-the-source-code)
2.   [Compile the source code](#2-compile-the-source-code)
3.   [Initialize the databases](#3-initialize-the-databases)
     * [DotStatSuiteCore_Common database](#initialize-the-dotstatsuitecore-common-database)
     * [Design DotStatSuiteCore_Data database](#initialize-one-design-dotstatsuitecore-data-database)
     * [Disseminate DotStatSuiteCore_Data database](#initialize-one-disseminate-dotstatsuitecore-data-database)
     * [Configure the maapi.net tool](#configure-the-maapi-net-tool)
     * [Design DotStatSuiteCore_Struct database (mappingstore db)](#initialize-one-design-dotstatsuitecore-struct-database-mappingstore-db)
     * [Disseminate DotStatSuiteCore_Struct database (mappingstore db)](#initialize-one-disseminate-dotstatsuitecore-struct-database-mappingstore-db)
4.   [Deploy the Transfer service](#4-deploy-the-transfer-service)
5.   [Deploy the Design NSI web service](#5-deploy-the-design-nsi-web-service-in-port-81)
6.   [Deploy the Disseminate NSI web service](#6-deploy-the-disseminate-nsi-web-service-in-port-80)

---

### 1 Download the source code

In this section we'll download the source code for the databases and for each of the applications.

#### A word on versions

The versions of the source code we download here need to work together. This means that we need to download compatible versions of all the various components, which is why none of the clone statements refer to branches such as "master" or "develop", but instead refer to tags we know to be compatible.

  1 .  Open Git Bash with **admin rights** from the windows start menu

  2 .  Create a new folder *C:/git* to store the source code
```sh 
mkdir /c/git
```

  3 .  Move to the new folder
```sh 
cd /c/git
```

  4 .  Clone the dotstatsuite-core-data-access repository.- *This repository contains the dotstatsuite-core-dbup tool, which will be used to create and initialize the common and data databases.*
```sh 
git clone -b 13.0.1 --single-branch https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access.git dotstatsuite-core-dbup
```

  5 .  Clone the maapi.net tool repository from the SIS-CC's mirror of Eurostat repository - *This tool will be used to initialize the structure databases.* 

```sh 

git clone -b 8.5.0 --single-branch https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored.git maapi.net
```

> **WARNING!** - This repository has a git submodule (authdb.sql) that points to the original ESTAT's repository in the mirror repository. To change the url of the submodule and to clone it manually (from the SIS-CC's mirror of Eurostat repository) use the following commands:  
>
>   ```sh
>   cd maapi.net
>   git submodule add https://gitlab.com/sis-cc/eurostat-sdmx-ri/authdb.sql.mirrored.git authdb
>   cd ..
>   ```
>

  6 .  Clone the NSI web service repository from the SIS-CC's mirror of Eurostat repository.

```sh
git clone -b 8.5.0 --single-branch https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored.git nsiws.net
```

  7 .  Clone the authorization.net repository from the SIS-CC's mirror of Eurostat repository.- *For authorization plugin.* 

```sh
git clone -b 8.5.0 --single-branch https://gitlab.com/sis-cc/eurostat-sdmx-ri/authorization.net.mirrored.git authorization.net
```

  8 .  Clone the dotstatsuite-core-transfer repository
```sh
git clone -b 8.0.1 --single-branch https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer.git
```

---

### 2 Compile the source code

  1 .  Compile the dotstatsuite-core-dbup tool
```sh
dotnet publish /c/git/dotstatsuite-core-dbup
```
  2 .  Compile the maapi.net tool
```sh
dotnet publish /c/git/maapi.net/src/Estat.Sri.Mapping.Tool/Estat.Sri.Mapping.Tool.csproj
```
  3 .  Compile the NSI web service

*WARNING:* The nsi webservice requires a long list of libraries that are downloaded at build time. Sometimes a single build is not enough to download all these libraries, therefore *make sure all resources are downloaded at build time, otherwise run the build command multiple times*

```sh
dotnet build /c/git/nsiws.net/NSIWebServices.sln
```

```sh
dotnet publish /c/git/nsiws.net/NSIWebServices.sln
```
  4 .  Compile the authorization plugin
```sh
dotnet publish /c/git/authorization.net/src/estat.sri.ws.auth.dotstat/estat.sri.ws.auth.dotstat.csproj
```
  5 .  Compile the dotstatsuite-core-transfer
```sh
dotnet publish /c/git/dotstatsuite-core-transfer
```

---

### 3 Initialize the databases

For this step you will need the Microsoft SQL sysadmin user and password (server admin when using Azure SQL Database or Azure SQL Managed Instance).  

#### Azure SQL Database pricing tier requirements
Please note that for .Stat *data* databases (e.g. *DesignDataDb* and *DisseminateDataDb* below) on Azure SQL Database the minimum pricing tier required is **Standard S3** because of the usage of columnstore indexes.

.Stat DbUp tool creates all databases at Standard S0 pricing on Azure SQL Database and the pricing tiers of *data* databases are upgraded to Standard S3.
Pricing tiers of *common* and *structure* databases remain at Standard S0.
At later updates DbUp will not change the pricing tier of any of the databases.

If the databases are not created by DbUp tool then the pricing tier of a *data* database is changed only to *Standard S3* if the already existing database is at *Basic*, *Standard S0*, *Standard S1* or *Standard S2* tier.
This upgrade of pricing tier is done only at the first run of DbUp tool, at later updates DbUp will not change the pricing tier of any of the databases.

After successful initialization of the databases the pricing tiers may be changed according to the needs, keeping in mind that the minimum required tier is *Standard S3* for .Stat *data* databases.

#### Initialize the DotStatSuiteCore_Common database  

![.Stat Core topology Common](/dotstatsuite-documentation/images/stat-core-topology-common.PNG)  

Execute the DbUp tool (*DotStat.DbUp.dll*) with the parameters to create and initialize the DotStatSuiteCore_Common database.

`Replace SA_USER and SA_PASSWORD` with the Microsoft SQL sysadmin credentials.
```sh
dotnet /c/git/dotstatsuite-core-dbup/DotStat.DbUp/bin/Debug/netcoreapp3.1/publish/DotStat.DbUp.dll upgrade --connectionString "Server=localhost;Database=CommonDb;User=SA_USER;Password=SA_PASSWORD;" --commonDb --loginName testLoginCommon --loginPwd "testLogin(\!)Password" --force
```

#### Initialize one Design DotStatSuiteCore_Data database  

![.Stat Core topology designData](/dotstatsuite-documentation/images/stat-core-topology-designData.PNG)  

Execute the Dbup tool (*DotStat.DbUp.dll*) with the parameters to create and initialize Design DotStatSuiteCore_Data database.

`Replace SA_USER and SA_PASSWORD` with the Microsoft SQL sysadmin credentials.
``` sh
dotnet /c/git/dotstatsuite-core-dbup/DotStat.DbUp/bin/Debug/netcoreapp3.1/publish/DotStat.DbUp.dll upgrade --connectionString "Server=localhost;Database=DesignDataDb;User=SA_USER;Password=SA_PASSWORD;" --dataDb --loginName testLoginDesignData --loginPwd "testLogin(\!)Password" --force
```

#### Initialize one Disseminate DotStatSuiteCore_Data database  

![.Stat Core topology disseminateData](/dotstatsuite-documentation/images/stat-core-topology-disseminateData.PNG)  

Execute the DbUp tool (*DotStat.DbUp.dll*) with the parameters to create and initialize the Disseminate DotStatSuiteCore_Data database.

`Replace SA_USER and SA_PASSWORD` with the Microsoft SQL sysadmin credentials.
```sh
dotnet /c/git/dotstatsuite-core-dbup/DotStat.DbUp/bin/Debug/netcoreapp3.1/publish/DotStat.DbUp.dll upgrade --connectionString "Server=localhost;Database=DisseminateDataDb;User=SA_USER;Password=SA_PASSWORD;" --dataDb  --loginName testLoginDisseminateData --loginPwd "testLogin(\!)Password" --force
```

#### Configure the maapi.net tool 

The maapi.net tool is used to initialize DotStatSuiteCore_Struct (mappingStore) databases. In this example we will use it to initialize two DotStatSuiteCore_Struct databases (design and disseminate).

The  tool requires a pre-configured list of connection strings of each of the databases that will be created and/or updated. This information should be added to the Estat.Sri.Mapping.Tool.dll.config file, under the section "\<connectionStrings\>".  

```xml
...
  <!--app.config -->
  <connectionStrings>
    <clear/>
    <add name="sqlserver" connectionString="Data Source=sodi-test;Initial Catalog=msdb_scratch;User Id=mauser;Password=123"
        providerName="System.Data.SqlClient"/>
    <add name="odp" connectionString="Data Source=sodi-test/xe;User ID=msdb_scratch;Password=123" providerName="Oracle.ManagedDataAccess.Client"/>
    <add name="mysql" connectionString="server=sodi-test;user id=mauser;password=123;database=msdb_scratch;default command timeout=120"
        providerName="MySql.Data.MySqlClient"/>
  </connectionStrings>
...
```

To simplify the process, we will use the configuration example [maapi-app.config](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/docs/installation/config-examples/maapi-app.config) file from dotstatsuite-core-sdmxri-nsi-ws repository.

-  Move to the maapi.net folder
```sh
cd /c/git/maapi.net/src/Estat.Sri.Mapping.Tool/bin/Debug/netcoreapp3.1/publish/
```

- From the dotstatsuite-core-sdmxri-nsi-ws repository, download the example configuration file `maapi-app.config` to the 
Estat.Sri.Mapping.Tool.dll.config 
```sh
curl https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/raw/master/docs/installation/config-examples/maapi-app.config?inline=false >Estat.Sri.Mapping.Tool.dll.config
```

#### Initialize one Design DotStatSuiteCore_Struct database (MappingStore db)  

![.Stat Core topology designStruct](/dotstatsuite-documentation/images/stat-core-topology-designStruct.PNG)

The initialization of MappingStore databases is done in two steps, first the dotstatsuite-core-dbup tool is used to create an empty database with the user and its credentials, and finally the maapi.net tool will generate all the remaining database artifacts.

**Step 1.** Create the empty database using the dotstatsuite-core-dbup tool.

Execute the Dbup tool (*DotStat.DbUp.dll*) with the parameters to create and initialize the Disseminate DotStatSuiteCore_Struct database.

`Replace SA_USER and SA_PASSWORD` with the Microsoft SQL sysadmin credentials.
``` sh
dotnet /c/git/dotstatsuite-core-dbup/DotStat.DbUp/bin/Debug/netcoreapp3.1/publish/DotStat.DbUp.dll upgrade --connectionString "Server=localhost;Database=DesignStructDb;User=SA_USER;Password=SA_PASSWORD;" --mappingStoreDb --loginName testLoginDesignStruct --loginPwd "testLogin(\!)Password" --force
```

**Step 2.**   Initialize the database using the maapi.net tool

```sh
dotnet Estat.Sri.Mapping.Tool.dll init -m DesignStructDb -f 
```

#### Initialize one Disseminate DotStatSuiteCore_Struct database (MappingStore db)  

![.Stat Core topology disseminateStruct](/dotstatsuite-documentation/images/stat-core-topology-disseminateStruct.PNG)
   
The initialization of MappingStore databases is done in two steps, first the dotstatsuite-core-dbup tool is used to create an empty database with the user and its credentials, and finally the maapi.net tool will generate all the remaining database artifacts.

**Step 1.** Create the empty database using the dotstatsuite-core-dbup tool.

Execute the Dbup tool (*DotStat.DbUp.dll*) with the parameters to create and initialize the Disseminate DotStatSuiteCore_Struct database.

`Replace SA_USER and SA_PASSWORD` with the Microsoft SQL sysadmin credentials.
```sh
dotnet /c/git/dotstatsuite-core-dbup/DotStat.DbUp/bin/Debug/netcoreapp3.1/publish/DotStat.DbUp.dll upgrade --connectionString "Server=localhost;Database=DisseminateStructDb;User=SA_USER;Password=SA_PASSWORD;" --mappingStoreDb --loginName testLoginDisseminateStruct --loginPwd "testLogin(\!)Password" --force
```

**Step 2.**   Initialize the database using the maapi.net tool

```sh
dotnet Estat.Sri.Mapping.Tool.dll init -m DisseminateStructDb -f 
```

---

### 4 Deploy the Transfer service  

![.Stat Core topology transfer](/dotstatsuite-documentation/images/stat-core-topology-transfer.PNG)

**Step 1.** Create a new folder to create the web service 

```sh
mkdir -p /c/dotstatsuite-website/transfer-service/
```

**Step 2.** Add full access rights to the users "IUSR" and "IIS_IUSRS" to the folder

```sh
icacls "C:\dotstatsuite-website\transfer-service" /grant:r "IUSR":"(OI)(CI)F"
icacls "C:\dotstatsuite-website\transfer-service" /grant:r "IIS_IUSRS":"(OI)(CI)F"
```
**Step 3.** Copy the compiled binaries to the new folder 

```sh
cp -r /c/git/dotstatsuite-core-transfer/DotStatServices.Transfer/bin/Debug/netcoreapp3.1/publish/* /c/dotstatsuite-website/transfer-service/
```

**Step 4.** Create a new IIS application called **transfer-service** in port 83, using [appcmd command](https://docs.microsoft.com/en-us/iis/get-started/getting-started-with-iis/getting-started-with-appcmdexe)
>  Make sure git bash is running in admin mode.
```sh
/c/Windows/System32/inetsrv/appcmd add site /name:transfer-service /physicalPath:C:\\dotstatsuite-website\\transfer-service /bindings:http/*:83:
```

**Step 5.** Create a new IIS application pool (TransferServiceAppPool) 
```sh
/c/Windows/System32/inetsrv/appcmd add apppool /name:TransferServiceAppPool /managedRuntimeVersion:"" /managedPipelineMode:Integrated
```
*  Add the transfer-service application to the newly created application pool  

```sh
/c/Windows/System32/inetsrv/appcmd set app "transfer-service/" /applicationPool:TransferServiceAppPool
```

**Step 6.** Configure the transfer service

There are two options to configure the transfer service: 

1.  Json config files.- `NOT RECOMMENDED`

>  Copy the sample file dataspaces.private.json from C:\git\dotstatsuite-core-transfer\docs\installation\config-examples to the deployment folder (C:\dotstatsuite-website\transfer-service\config). 

2.  Saving the configuration setting as environment variables for the IIS site (transfer-service). `Recommended`

>  [See more about the configuration settings](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/blob/master/README.md#configuration).

For this example we will use the second option: 

*  Set the common database connection string:
```sh
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='DotStatSuiteCoreCommonDbConnectionString',value='Data Source=localhost;Initial Catalog=CommonDb;User ID=testLoginCommon;Password=testLogin(\!)Password']" /commit:apphost
```

* Set the default language code:
```sh
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='DefaultLanguageCode',value='en']" /commit:apphost
```

*  Disable authentication:
```sh
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__enabled',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__claimsMapping__email',value='null']" /commit:apphost
```

**NOTE** that the transfer service will not send emails (for actions performed on data from the .Stat DLM) until authentication is enabled and properly configured. To **enable authentication** in the transfer-service, see [.Stat authentication configuration](https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/authentication/).

*  Set the design dataspace values:
```sh
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__0__Id',value='design']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__0__DotStatSuiteCoreStructDbConnectionString',value='Data Source=localhost;Initial Catalog=DesignStructDb;User ID=testLoginDesignStruct;Password=testLogin(\!)Password']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__0__DotStatSuiteCoreDataDbConnectionString',value='Data Source=localhost;Initial Catalog=DesignDataDb;User ID=testLoginDesignData;Password=testLogin(\!)Password']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__0__DataImportTimeOutInMinutes',value='60']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__0__DatabaseCommandTimeoutInSec',value='360']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__0__AutoLog2DB',value='true']" /commit:apphost

```
*  Set the disseminate dataspace values:
```sh
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__1__Id',value='disseminate']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__1__DotStatSuiteCoreStructDbConnectionString',value='Data Source=localhost;Initial Catalog=DisseminateStructDb;User ID=testLoginDisseminateStruct;Password=testLogin(\!)Password']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__1__DotStatSuiteCoreDataDbConnectionString',value='Data Source=localhost;Initial Catalog=DisseminateDataDb;User ID=testLoginDisseminateData;Password=testLogin(\!)Password']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__1__DataImportTimeOutInMinutes',value='60']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__1__DatabaseCommandTimeoutInSec',value='360']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "transfer-service" -section:system.webServer/aspNetCore /+"environmentVariables.[name='spacesInternal__1__AutoLog2DB',value='true']" /commit:apphost
```

**NOTE** [AutoLog2DB](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer#spacesinternal) is enabled for both dataspaces. This configuration allows the transfer service to store logs in the database. When this setting is enabled, the functions [/status/request](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer#post-12statusrequest-get-the-request-information-by-transaction-id-and-dataspace) and [/status/requests](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer#post-12allstatusrequests-query-the-status-all-the-requests-and-their-logs) can be used to retrieve logs.

**Step 7.** Start the new application
```sh
/c/Windows/System32/inetsrv/appcmd start site /site.name:transfer-service
```

**Step 8.** Test that the application is up and running  

*  **Using curl**
```sh
curl localhost:83/health
```

*  **Using a web browser**  
Open a web browser and open the url localhost:83/health

**You should see similar result:**
```json
{
  "service": {
    "status": "Healthy",
    "details": {
      "version": "5.0.96+dc43333a28",
      "auth_enabled": false
    },
    "responseTime": 7.1725
  },
  "database": {
    "status": "Healthy",
    "details": {
      "design": {
        "structureDbVersion": "6.12",
        "dataDbVersion": "3.6"
      },
      "disseminate": {
        "structureDbVersion": "6.12",
        "dataDbVersion": "3.6"
      }
    },
    "responseTime": 243.1098
  },
  "memory": {
    "status": "Healthy",
    "details": {
      "allocatedMb": 11.0912,
      "gen0Collections": 1,
      "gen1Collections": 0,
      "gen2Collections": 0
    },
    "responseTime": 3.5734
  },
  "totalResponseTime": 250.5597
}
```

>  Note: By default all the logs will be stored at C:\dotstatsuite-website\transfer-service\logs\

---

### 5 Deploy the Design NSI web service in port 81  

![.Stat Core topology nsiwsDesign](/dotstatsuite-documentation/images/stat-core-topology-nsiwsDesign.PNG)

**Step 1.** Create a new folder to create the web service 

```sh
mkdir -p /c/dotstatsuite-website/nsiws-design/
```

**Step 2.** Add full access rights to the users "IUSR" and "IIS_IUSRS" to the folder

```sh
icacls "C:\dotstatsuite-website\nsiws-design" /grant:r "IUSR":"(OI)(CI)F"
icacls "C:\dotstatsuite-website\nsiws-design" /grant:r "IIS_IUSRS":"(OI)(CI)F"
```

**Step 3.** Copy the compiled binaries to the new folder 
```sh
cp -r /c/git/nsiws.net/src/NSIWebServiceCore/bin/Debug/netcoreapp3.1/publish/* /c/dotstatsuite-website/nsiws-design/
```

**Step 4.** Copy the following binaries from authorization.net to the *Plugins* folder. 
This will allow the NSI web service to retrieve authorization rules from .Stat common database when the authorization is enabled.

- DotStat.Common.dll
- DotStat.DB.dll
- DotStat.Domain.dll
- DotStat.MappingStore.dll
- estat.sri.ws.auth.dotstat.dll
- estat.sri.ws.auth.dotstat.deps.json

```sh
cp -r /c/git/authorization.net/src/estat.sri.ws.auth.dotstat/bin/Debug/netstandard2.1/publish/{DotStat.Common.dll,DotStat.DB.dll,DotStat.Domain.dll,DotStat.MappingStore.dll,estat.sri.ws.auth.dotstat.dll,estat.sri.ws.auth.dotstat.deps.json} /c/dotstatsuite-website/nsiws-design/Plugins
```

**Step 5.** Configure the nsi web service 
From the dotstatsuite-core-sdmxri-nsi-ws repository, download the following sample configuration files to the deployment folder:
*  [nsiws-design-app.config](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/docs/installation/config-examples/nsiws-design-app.config) to the file */config/app.config* 

>  This sample configuration file has been set to use the databases and users that were previously created in this guide. [See more about the configuration settings](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/readme.md).

```sh
curl https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/raw/master/docs/installation/config-examples/nsiws-design-app.config?inline=false >/c/dotstatsuite-website/nsiws-design/config/app.config
```

*  **NEW - Set data database connection string**
Modify the configuration file /config/Properties.json - Add the value for the disseminationDbConnection connectionString:

   - Change From:
   
```json
  "disseminationDbConnection": {
    "dbType": "SqlServer",
    "connectionString": ""
  }
```

   - To
   
```json
  "disseminationDbConnection": {
    "dbType": "SqlServer",
    "connectionString": "Data Source=localhost;Initial Catalog=DesignDataDb;User ID=testLoginDesignData;Password=testLogin(\!)Password"
  }
```

>  **Logging configuration**.- By default, the service has been configured to log all activity. **`This causes performance issues during data extractions`**. To avoid performance issues on production envirements, please make the following changes to the file */config/log4net.config*
 
 In line 80 modify the xml block FROM:
```xml
...
  <root>
    <level value="ALL"/>
    <appender-ref ref="SystemActivityAppender"/>
    <appender-ref ref="UserActivityAppender"/>
  </root>
...
```

TO:
```xml
...
  <root>
    <level value="WARN"/>
    <appender-ref ref="SystemActivityAppender"/>
    <appender-ref ref="UserActivityAppender"/>
  </root>
...
```

**Step 6.** Create a new IIS application called **nsiws-design** in port 81, using [appcmd command](https://docs.microsoft.com/en-us/iis/get-started/getting-started-with-iis/getting-started-with-appcmdexe)

>  Make sure git bash is running in admin mode.

```sh
/c/Windows/System32/inetsrv/appcmd add site /name:nsiws-design /physicalPath:C:\\dotstatsuite-website\\nsiws-design /bindings:http/*:81:
```

**Step 6.** Create a new IIS application pool (NSIWSDesignAppPool) 
```sh
/c/Windows/System32/inetsrv/appcmd add apppool /name:NSIWSDesignAppPool /managedRuntimeVersion:"" /managedPipelineMode:Integrated
```
*  Add the nsiws-design application to the newly created application pool  
```sh
/c/Windows/System32/inetsrv/appcmd set app "nsiws-design/" /applicationPool:NSIWSDesignAppPool
```

**Step 8.** Configure the authentication and the authorization plugin for retrieval of .Stat authorization rules

There are two options to configure the authorization plugin:  
  1 .  Json config file.- `NOT RECOMMENDED`

>  Download the sample files [nsiws-authentication.json](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/docs/installation/config-examples/nsiws-authentication.json) and [nsiws-design-authorization.json](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/docs/installation/config-examples/nsiws-design-authorization.json) from *dotstatsuite-core-sdmxri-nsi-ws* repository to the deployment folder (C:\dotstatsuite-website\nsiws-design\config). 

  2 .  Saving the configuration setting as environment variables for the IIS site (nsiws-design). `Recommended`

>  [See more about the configuration settings](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/readme.md).

For this example we will use the second option: 

*  Set authentication turned OFF
```sh
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__enabled',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__allowAnonymous',value='true']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__requireHttps',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__validateIssuer',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__showPii',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__clientId',value='stat-suite']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__authority',value='']" /commit:apphost
```

*  Set authorization turned ON
```sh
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='authorization__enabled',value='true']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='authorization__method',value='dotstat']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='authorization__PrincipalFrom',value='context']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='DotStatSuiteCoreCommonDbConnectionString',value='Data Source=localhost;Initial Catalog=CommonDb;User ID=testLoginCommon;Password=testLogin(\!)Password']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='mappingStore__Id__Default',value='design']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-design" -section:system.webServer/aspNetCore /+"environmentVariables.[name='mappingStore__Id__FromQueryParameter',value='Never']" /commit:apphost
```

Please note that with the configuration above the NSI WS instance is configured to allow only anonymous access (as authentication is turned off).
The authorization rules can be managed via Authorization Management service (e.g. using the Swagger UI of the service).
Permissions granted to anonymous user is controlled by rules assigned to all users (userId="*"). 

**Step 9.** Start the new application
```sh
/c/Windows/System32/inetsrv/appcmd start site /site.name:nsiws-design
```

**Step 10.** Test that the application is up and running


*  **Using curl**
```sh
curl localhost:81/health
```

*  **Using a web browser**  
Open a web browser and open the url localhost:81/health

**You should see similar result:**
```json
{
  "service": {
    "status": "Healthy",
    "details": {
      "version": "8.1.2",
      "retriever": "MappingStoreRetrieversFactory",
      "retriever-version": "8.1.2",
      "middleware": "CorsMiddlewareBuilder,OpenIdMiddlewareBuilder,LoggingOptionsBuilder,UserAuthorizationRulesMiddlerwareBuilder",
      "maxRequestBodySize": 30000000
    },
    "responseTime": 6.1152
  },
  "db": {
    "status": "Healthy",
    "details": {
      "storeId": "design",
      "version": "6.12",
      "isLatest": true
    },
    "responseTime": 252.2288
  },
  "memory": {
    "status": "Healthy",
    "details": {
      "allocatedMb": 9.3896,
      "gen0Collections": 1,
      "gen1Collections": 0,
      "gen2Collections": 0
    },
    "responseTime": 5.1278
  },
  "totalResponseTime": 401.745
}
```

>  Note: By default all the logs will be stored at C:/ProgramData/Eurostat/logs/

---

### 6 Deploy the Disseminate NSI web service in port 80  

![.Stat Core topology nsiwsDisseminate](/dotstatsuite-documentation/images/stat-core-topology-nsiwsDisseminate.PNG)

**Step 1.** Create a new folder to create the web service 

```sh
mkdir -p /c/dotstatsuite-website/nsiws-disseminate/
```

**Step 2.** Add full access rights to the users "IUSR" and "IIS_IUSRS" to the folder

```sh
icacls "C:\dotstatsuite-website\nsiws-disseminate" /grant:r "IUSR":"(OI)(CI)F"
icacls "C:\dotstatsuite-website\nsiws-disseminate" /grant:r "IIS_IUSRS":"(OI)(CI)F"
```

**Step 3.** Copy the compiled binaries to the new folder 
```sh
cp -r /c/git/nsiws.net/src/NSIWebServiceCore/bin/Debug/netcoreapp3.1/publish/* /c/dotstatsuite-website/nsiws-disseminate/
```

**Step 4.** Copy the following binaries from authorization.net to the *Plugins* folder. 
This will allow the NSI web service to retrieve authorization rules from .Stat common database when the authorization is enabled.

- DotStat.Common.dll
- DotStat.DB.dll
- DotStat.Domain.dll
- DotStat.MappingStore.dll
- estat.sri.ws.auth.dotstat.dll
- estat.sri.ws.auth.dotstat.deps.json

```sh
cp -r /c/git/authorization.net/src/estat.sri.ws.auth.dotstat/bin/Debug/netstandard2.1/publish/{DotStat.Common.dll,DotStat.DB.dll,DotStat.Domain.dll,DotStat.MappingStore.dll,estat.sri.ws.auth.dotstat.dll,estat.sri.ws.auth.dotstat.deps.json} /c/dotstatsuite-website/nsiws-disseminate/Plugins
```

**Step 5.** Configure the nsi web service  
From the dotstatsuite-core-sdmxri-nsi-ws repository, download the following sample configuration files to the deployment folder:
*  [nsiws-disseminate-app.config](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/docs/installation/config-examples/nsiws-disseminate-app.config) to the file */config/app.config* 

>  This sample configuration file has been set to use the databases and users that were previously created in this guide. [See more about the configuration settings](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/readme.md).

```sh
curl https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/raw/master/docs/installation/config-examples/nsiws-disseminate-app.config?inline=false >/c/dotstatsuite-website/nsiws-disseminate/config/app.config
```
*  **NEW - Set data database connection string**
Modify the configuration file /config/Properties.json - Add the value for the disseminationDbConnection connectionString:

   - Change From:
   
```json
  "disseminationDbConnection": {
    "dbType": "SqlServer",
    "connectionString": ""
  }
```

   - To
   
```json
  "disseminationDbConnection": {
    "dbType": "SqlServer",
    "connectionString": "Data Source=localhost;Initial Catalog=DisseminateDataDb;User ID=testLoginDisseminateData;Password=testLogin(\!)Password"
  }
```


>  **Logging configuration**.- By default, the service has been configured to log all activity. **`This causes performance issues during data extractions`**. To avoid performance issues on production envirements, please make the following changes to the file */config/log4net.config*
 
 In line 80 modify the xml block FROM:
```xml
...
  <root>
    <level value="ALL"/>
    <appender-ref ref="SystemActivityAppender"/>
    <appender-ref ref="UserActivityAppender"/>
  </root>
...
```

TO:
```xml
...
  <root>
    <level value="WARN"/>
    <appender-ref ref="SystemActivityAppender"/>
    <appender-ref ref="UserActivityAppender"/>
  </root>
...
```

**Step 6.** Create a new IIS application called **nsiws-disseminate** in port 80, using [appcmd command](https://docs.microsoft.com/en-us/iis/get-started/getting-started-with-iis/getting-started-with-appcmdexe)  

>  Make sure git bash is running in admin mode.

```sh
/c/Windows/System32/inetsrv/appcmd add site /name:nsiws-disseminate /physicalPath:C:\\dotstatsuite-website\\nsiws-disseminate /bindings:http/*:80:
```

**Step 7.** Create a new IIS application pool (NSIWSDisseminateAppPool) 
```sh
/c/Windows/System32/inetsrv/appcmd add apppool /name:NSIWSDisseminateAppPool /managedRuntimeVersion:"" /managedPipelineMode:Integrated
```  

*  Add the nsiws-disseminate application to the newly created application pool
```sh
/c/Windows/System32/inetsrv/appcmd set app "nsiws-disseminate/" /applicationPool:NSIWSDisseminateAppPool
```

**Step 8.** Configure the authentication and the authorization plugin for retrieval of .Stat authorization rules

There are two options to configure the authorization plugin:  
  1 .  Json config file.- `NOT RECOMMENDED`

>  Download the sample files [nsiws-authentication.json](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/docs/installation/config-examples/nsiws-authentication.json) and [nsiws-design-authorization.json](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/docs/installation/config-examples/nsiws-design-authorization.json) from *dotstatsuite-core-sdmxri-nsi-ws* repository to the deployment folder (C:\dotstatsuite-website\nsiws-disseminate\config). 

  2 .  Saving the configuration setting as environment variables for the IIS site (nsiws-disseminate). `Recommended`

>  [See more about the configuration settings](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/blob/master/readme.md).

For this example we will use the second option:   

*  Set authentication turned OFF
```sh
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__enabled',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__allowAnonymous',value='true']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__requireHttps',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__validateIssuer',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__showPii',value='false']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__clientId',value='stat-suite']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='auth__authority',value='']" /commit:apphost
```

*  Set authorization turned ON
```sh
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='authorization__enabled',value='true']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='authorization__method',value='dotstat']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='authorization__PrincipalFrom',value='context']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='DotStatSuiteCoreCommonDbConnectionString',value='Data Source=localhost;Initial Catalog=CommonDb;User ID=testLoginCommon;Password=testLogin(\!)Password']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='mappingStore__Id__Default',value='disseminate']" /commit:apphost
/c/Windows/System32/inetsrv/appcmd set config "nsiws-disseminate" -section:system.webServer/aspNetCore /+"environmentVariables.[name='mappingStore__Id__FromQueryParameter',value='Never']" /commit:apphost
```

Please note that with the configuration above the NSI WS instance is configured to allow only anonymous access (as authentication is turned off).
The authorization rules can be managed via Authorization Management service (e.g. using the Swagger UI of the service).
Permissions granted to anonymous user is controlled by rules assigned to all users (userId="*"). 

**Step 9.** Start the new application
```sh
/c/Windows/System32/inetsrv/appcmd start site /site.name:nsiws-disseminate
```

**Step 10.** Test that the application is up and running

*  **Using curl**
```sh
curl localhost:80/health
```

*  **Using a web browser**  
Open a web browser and open the url localhost:80/health

**You should see similar result:**
```json
{
  "service": {
    "status": "Healthy",
    "details": {
      "version": "8.1.2",
      "retriever": "MappingStoreRetrieversFactory",
      "retriever-version": "8.1.2",
      "middleware": "CorsMiddlewareBuilder,OpenIdMiddlewareBuilder,LoggingOptionsBuilder,UserAuthorizationRulesMiddlerwareBuilder",
      "maxRequestBodySize": 30000000
    },
    "responseTime": 6.0177
  },
  "db": {
    "status": "Healthy",
    "details": {
      "storeId": "disseminate",
      "version": "6.12",
      "isLatest": true
    },
    "responseTime": 245.2298
  },
  "memory": {
    "status": "Healthy",
    "details": {
      "allocatedMb": 9.5320,
      "gen0Collections": 1,
      "gen1Collections": 0,
      "gen2Collections": 0
    },
    "responseTime": 3.7186
  },
  "totalResponseTime": 355.0478
}
```

>  Note: By default all the logs will be stored at C:/ProgramData/Eurostat/logs/

---

### 7. Additional IIS configuration
#### 7.1 Changing maximum lenght of URL segments
*Symptoms*:  
A valid SDMX data query with many query filter values produces the following error, even though the length of the url is below 4096 characters:
```
400 Bad request
HTTP Error 400. The request URL is invalid.
```

When the filter section of the query is shorter (than 260 characters) or removed, then the request produces regular response.

*Solution*:  
When using IIS, the maximum number of characters in a URL path segment (the area between the slashes in the URL) is controled by a *[HTTP.sys](https://docs.microsoft.com/en-us/troubleshoot/iis/httpsys-registry-windows)* configuration parameter called `UrlSegmentMaxLength`.  
This can be changed in `HKLM\SYSTEM\CurrentControlSet\Services\HTTP\Parameters` with `DWORD` value `UrlSegmentMaxLength`.  
The default value of this parameter (when this value is missing from registry) is 260, so by default this restriction is applied on IIS.

**Increasing this parameter to a larger value** (e.g. to 2048) **resolves the issue reported.**  
If value is set to zero, then the length is bounded by the maximum value of a ULONG, but having no limitation on this value is not recommended.
Please note that other settings may still limit the entire url, e.g. maximum number of url segments or maximum url length.

Please also note that after changing this value in registry, the **restart** of the Windows machine is **required**.
