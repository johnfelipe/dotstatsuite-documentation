---
title: "Upload data from Excel files"
subtitle: 
comments: false
weight: 290

---

The Data Lifecycle Manager allows uploading data stored in **Excel xlsx** files, if the data is organised in spreadsheet tables according to a specific table description, which is to be provided together with the Excel file through an additional "EDD" (Excel Data Descriptor) xml file.

- EDD schema: [EDD.xsd](/dotstatsuite-documentation/using-dlm/upload-data/EDD.xsd)
- EDD documentation: [EDD-documentation-v1.0.docx](/dotstatsuite-documentation/using-dlm/upload-data/EDD-documentation-v1.0.docx)
- Example-based explanation: [EDD-documentation-v1.0.xlsx](/dotstatsuite-documentation/using-dlm/upload-data/EDD-documentation-v1.0.xlsx)
- Example Excel xlsx and EDD: 
  - Excel data: [Excel-data-example.xlsx](/dotstatsuite-documentation/using-dlm/upload-data/Excel-data-example.xlsx)
  - EDD: [EDD-example.xml](/dotstatsuite-documentation/using-dlm/upload-data/EDD-example.xml)
