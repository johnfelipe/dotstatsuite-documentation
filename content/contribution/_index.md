---
title: "Contributing"
subtitle: 
comments: false
weight: 100
keywords: [
  'Report an issue', 'https://sis-cc.gitlab.io/dotstatsuite-documentation/contribution/report-an-issue',
  'Issue process & Definitions of Done', 'https://sis-cc.gitlab.io/dotstatsuite-documentation/contribution/issue-process',
  'Development guidelines', 'https://sis-cc.gitlab.io/dotstatsuite-documentation/contribution/development-guidelines',
  'Team members', 'https://sis-cc.gitlab.io/dotstatsuite-documentation/contribution/team-members',
]
---

includes:

* [Report an issue](/dotstatsuite-documentation/contribution/report-an-issue)
* [Issue process & Definitions of Done](/dotstatsuite-documentation/contribution/issue-process)
* [Development guidelines](/dotstatsuite-documentation/contribution/development-guidelines)
* [Team members](/dotstatsuite-documentation/contribution/team-members)
